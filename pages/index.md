---
mount: /
name: Home
toc: off
timestamp: null
template: frontpage.html
redirects:
  - cards

revised:
  by: Jenny Tran
  date: 2024-11-25

card_list:
- switch: "admin"
  label: "Administrative processes, Staff and Campus"
  icon: "1f5c2.svg"
  categories:
  - label: "General"
    icon: "1f30d.svg"
    link: "/tag/general"
    external: true
  - label: "On&Off-boarding"
    icon: "1f501.svg"
    link: "/tag/on-offboarding"
    external: true
  - label: "Financing and grants"
    icon: "1fa99.svg"
    link: "/tag/finance"
  - label: "Policies"
    icon: "1f4dc.svg"
    link: "/tag/policies"
  - label: "LCSB Handbook"
    icon: "1f4d5.svg"
    link: "/tag/handbook"
  - label: "Event organization"
    icon: "1f5d3.svg"
    link: "/tag/events"
- switch: "it"
  label: "Bioinformatics, Computational methods and IT"
  icon: "1f5a5.svg"
  categories:
  - label: "IT systems access"
    icon: "1faaa.svg"
    link: "/tag/access"
    external: true
  - label: "Data backup"
    icon: "1f4bd.svg"
    link: "/tag/backup"
    external: true
  - label: "Data sharing and exchange channels"
    icon: "1f4e1.svg"
    link: "/tag/exchange-channels"
    external: true
  - label: "Integrity"
    icon: "2728.svg"
    link: "/tag/integrity"
    external: true
  - label: "GDPR"
    icon: "1f6e1.svg"
    link: "/tag/gdpr"
  - label: "Publishing code and programs"
    icon: "1f9d1-200d-1f4bb.svg"
    link: "/tag/publication/code"
    external: true
  - label: "Contribute to How-to Cards"
    icon: "270f.svg"
    link: "/tag/contribute"
    external: true
- switch: "lab"
  label: "Wetlab equipment and processes"
  icon: "1f9d1-200d-1f52c.svg"
  categories:
  - label: "Access"
    icon: "1f6a6.svg"
    link: "/tag/lab/access"
  - label: "Biologicals"
    icon: "1f9eb.svg"
    link: "/tag/lab/biologicals"
  - label: "Chemicals"
    icon: "1f9ea.svg"
    link: "/tag/lab/chemicals"
    external: true
  - label: "Emergency procedures"
    icon: "1f6a8.svg"
    link: "/tag/lab/emergency"
    external: true
  - label: "Equipment"
    icon: "1f52c.svg"
    link: "/tag/lab/equipment"
    external: true
  - label: "Genetically Modified Organisms"
    icon: "1f9ec.svg"
    link: "/tag/lab/gmo"
  - label: "Good practices"
    icon: "2705.svg"
    link: "/tag/lab/good-practice"
    external: true
  - label: "Personal protective equipment"
    icon: "1f97c.svg"
    link: "/tag/lab/ppe"
    external: true
  - label: "Procurement"
    icon: "1f6d2.svg"
    link: "/tag/lab/procurement"
  - label: "Software"
    icon: "lab-software.svg"
    link: "/tag/lab/software"
    external: true
  - label: "Transport"
    icon: "1f69a.svg"
    link: "/tag/lab/transport"
    external: true
  - label: "Waste"
    icon: "267b.svg"
    link: "/tag/lab/waste"
    external: true
- switch: "publication"
  label: "Publication and Responsible Reproducible Research"
  icon: "1f4dd.svg"
  categories:
  - label: "Publishing processes"
    icon: "2328.svg"
    link: "/tag/publication/process"
  - label: "Pre-Publication Check"
    icon: "ppc.svg"
    link: "/tag/publication/ppc"
  - label: "Useful tools and resources"
    icon: "2712.svg"
    link: "/tag/publication/resources"
    external: true
- switch: "platforms"
  label: "Scientific Platforms"
  icon: "platforms.svg"
  categories:
  - label: "Metabolomics and Lipidomics Platform"
    icon: "metabolomics.svg"
    link: "/tag/platforms/metabolomics"
  - label: "Rodent facility"
    icon: "1f401.svg"
    link: "/tag/platforms/rodent-facility"
---
